package com.example.sdaFinalProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomenestFurnitureOnlineShopApplication {

	public static void main(String[] args) {
		SpringApplication.run(HomenestFurnitureOnlineShopApplication.class, args);
	}
}
