package com.example.sdaFinalProject.controller;

import com.example.sdaFinalProject.model.Product;
import com.example.sdaFinalProject.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping
public class MainController {
    @Autowired
    private UserController userController;

    @Autowired
    private ProductRepository productRepository;

    @GetMapping("/index")
    public ModelAndView getIndexPage() {
        List<Product> productList = productRepository.findAll();
        ModelAndView modelAndView = new ModelAndView("index");
        modelAndView.addObject("products", productList);
        return modelAndView;
    }
    @GetMapping("/")
    public String getBsePage() {
       return "redirect:/index";
    }

    @GetMapping("/loginPage")
    public String login(){
        return "login";
    }

    @RequestMapping("/loginError")
    public String loginError(Model model) {
        model.addAttribute("login-error", true);
        return "login-error";
    }

    @GetMapping("/about")
    public String aboutUs() {
        return "about";
    }

    @GetMapping("/contact")
    public String contact() {
        return "contact";
    }

}

