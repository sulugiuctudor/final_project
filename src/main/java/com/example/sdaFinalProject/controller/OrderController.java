package com.example.sdaFinalProject.controller;

import com.example.sdaFinalProject.model.*;
import com.example.sdaFinalProject.repository.OrderRepository;
import com.example.sdaFinalProject.service.OrderService;
import com.example.sdaFinalProject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Controller
public class OrderController {
    private final OrderService orderService;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private UserService userService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    /**
     * Show list of orders
     *
     * @return
     */
    @GetMapping("/orders")
    public ModelAndView showOrders(@RequestParam Optional<String> search) {
        ModelAndView modelAndView = new ModelAndView("order/list");

        List<Order> listOrders = new ArrayList<>();

        User user = UserController.curentUser();



        if (search.isPresent()) {
            try {
                Optional<Order> order = orderRepository.findById(Long.parseLong(search.get()));
                if (order.isPresent()) {
                    listOrders.add(order.get());
                }
            } catch (NumberFormatException nfe) {
                modelAndView.addObject("incorrect_number", true);
                System.out.println("Not a valid number");
            }
            if (listOrders.isEmpty()) {
                modelAndView.addObject("empty_result", true);
            }
            modelAndView.addObject("search", search.get());
        } else {
            listOrders = orderRepository.findAll();
        }

        //Sort elements to list for only loged user

        List<Order> templist = new ArrayList<>();

        for (Order ord:listOrders) {
            if(!user.getRole().equals("ADMIN")) {
                if (user.getId() == ord.getUser().getId()) {
                    templist.add(ord);
                }

            }else{

                modelAndView.addObject("orders", listOrders);
                return modelAndView;
            }
        }
        listOrders.clear();
        listOrders.addAll(templist);

        //------>Finish sort

        modelAndView.addObject("orders", listOrders);
        return modelAndView;
    }


    /**
     * Display update order form
     *
     * @param id
     * @return
     */
    @GetMapping("/update-order/{id}")
    public ModelAndView viewOrder(@PathVariable long id) {
        ModelAndView modelAndView = new ModelAndView("order/update");
        Order order = orderRepository.findById(id).get();
        modelAndView.addObject("order", order);
        modelAndView.addObject("statuses", OrderEnum.values());
        return modelAndView;
    }

    /**
     * Save updated order
     *
     * @param order
     * @return
     */
    @PostMapping("/update-order")
    public String saveUpdatedOrder(@ModelAttribute("updateOrder") Order order) {
        orderService.updateOrder(order);
        return "redirect:/orders";
    }

    @PostMapping("/create-order")
    public String placeOrder(@ModelAttribute ShoppingCart cart, HttpServletRequest request) {
        Order order = new Order();
        order.setOrderDate(new Date());
        order.setUser(userService.curentUser());
        order.setStatus(String.valueOf(OrderEnum.PLACED));
        order.setTotalValue(cart.getTotalValue());
        order.setShippingName(cart.getShippingName());
        order.setShippingAddress(cart.getShippingAddress());
        order = orderService.createOrder(order);

        Set<OrderProduct> orderProducts = new HashSet<>();
        for (ShoppingCartProduct scp : cart.getShoppingCartProducts()) {
            orderProducts.add(new OrderProduct(order, scp.getProduct(), scp.getQuantity()));
        }
        order.setOrderProductSet(orderProducts);
        orderService.updateOrder(order);

        request.getSession().setAttribute("myCart", new ShoppingCart());
        return "redirect:/";
    }

    /**
     * Delete order by id and redirect order list
     *
     * @param id
     * @return
     */
    @GetMapping("/delete-order/{id}")
    public ModelAndView deleteOrder(@PathVariable("id") Long id) {
        ModelAndView modelAndView = new ModelAndView("redirect:/orders");
        orderService.deleteOrder(id);
        return modelAndView;
    }

}
