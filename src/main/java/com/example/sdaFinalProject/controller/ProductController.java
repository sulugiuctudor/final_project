package com.example.sdaFinalProject.controller;

import com.example.sdaFinalProject.model.Product;
import com.example.sdaFinalProject.repository.ProductRepository;
import com.example.sdaFinalProject.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;


import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
public class ProductController {
    @Autowired
    private ProductRepository productRepository;

    private List<Product> searchProductList;

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;

    }

    /**
     * Show list of product
     *
     * @return
     */
    @GetMapping("/productList")
    public ModelAndView getAllProducts(ModelMap modelMap) {
        var modelAndView = new ModelAndView("product/list");
        modelAndView.addObject(productService.getAllProducts());
        return modelAndView;
    }

    @PostMapping("/productList")
    public ModelAndView searchProduct() {
        ModelAndView modelAndView = new ModelAndView("productSearchResult");
        modelAndView.addObject("productSearchResult", searchProductList);
        return modelAndView;
    }

    @GetMapping("/productSearch")
    public ModelAndView productSearch() {
        ModelAndView modelAndView = new ModelAndView("productSearchResult");
        modelAndView.addObject("productSearchResult", searchProductList);
        return modelAndView;

    }

    /**
     * Create product / upload picture
     *
     * @return
     */


    @PostMapping("/create-product")
    public String createProduct(@ModelAttribute(name = "productForm") @Valid Product product, BindingResult bindingResult, @RequestParam("file") MultipartFile file) {
        productService.createProduct(product, file);


        if (bindingResult.hasErrors()) {
            return "product/newOrUpdate";
        }

        return "redirect:/productList";
    }

    @GetMapping("/create-product")
    public ModelAndView getProductForm(ModelMap modelmap) {
        modelmap.addAttribute("productForm", new Product());
        modelmap.addAttribute("action", "create");
        return new ModelAndView("product/newOrUpdate");
    }

    /**
     * Save updated
     *
     * @param product
     * @return
     */

    @PostMapping("/updateOrder")
    public String postUpdateOrder(@ModelAttribute("updateOrder") Product product, @RequestParam("file") MultipartFile file) {
        productService.updateProduct(product, file);
        return "redirect:/productList";
    }

    /**
     * Save products
     *
     * @param product
     * @return
     */

    @PostMapping("/saveProducts")
    public String saveProducts(@ModelAttribute("saveProduct") Product product) {
        productService.saveProduct(product);
        return "product/list";
    }

    /**
     * Display update product form
     *
     * @param id
     * @return
     */

    @GetMapping("/update-product/{id}")
    public ModelAndView getProductForm(ModelMap modelmap,
                                       @PathVariable("id") long id) {
        Optional<Product> product = productRepository.findById(id);
        if (product.isPresent()) {
            modelmap.addAttribute("productForm", product.get());
        }
        modelmap.addAttribute("action", "update");
        //return html create client
        return new ModelAndView("product/newOrUpdate");
    }


    /**
     * update product form saveProducts
     *
     * @param id
     * @return
     */



    @PostMapping("/update-product/{id}")
    public String saveProducts(@ModelAttribute("updateProduct") @Valid Product product, BindingResult bindingResult,
                               @PathVariable("id") long id, @RequestParam("file") MultipartFile file) {
        productService.updateProduct(product, file);

        if (bindingResult.hasErrors()) {
            return "product/newOrUpdate";
        }

        return "redirect:/productList";
    }

    /**
     * Delete product by id and redirect product list
     *
     * @param id
     * @return
     */

    @RequestMapping(value = "/delete-product/{id}", method = {RequestMethod.DELETE, RequestMethod.GET})
    public String deleteProduct(@PathVariable("id") Long id) {
        productService.deleteProduct(id);
        return "redirect:/productList";
    }

    /**
     * Show product/list"
     *
     * @return
     */

    @GetMapping("/products")
    public ModelAndView showProducts() {
        ModelAndView modelAndView = new ModelAndView("product/list");
        List<Product> listProduct = productRepository.findAll();

        modelAndView.addObject("products", listProduct);
        return modelAndView;

    }

    /**
     * Show product I'd
     *
     * @param id
     * @return
     */

    @GetMapping("/product/{id}")
    public ModelAndView showProduct(@PathVariable("id") Long id) {
        ModelAndView modelAndView = new ModelAndView("product/view");
        Optional<Product> product = productRepository.findById(id);
        if (product.isPresent()) {
            modelAndView.addObject("product", product.get());
        }
        return modelAndView;
    }

}
