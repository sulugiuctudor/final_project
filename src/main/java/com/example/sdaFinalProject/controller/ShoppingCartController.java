package com.example.sdaFinalProject.controller;

import com.example.sdaFinalProject.model.ShoppingCart;
import com.example.sdaFinalProject.model.User;
import com.example.sdaFinalProject.repository.ProductRepository;
import com.example.sdaFinalProject.repository.UserRepository;
import com.example.sdaFinalProject.service.ShoppingCartService;
import com.example.sdaFinalProject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class ShoppingCartController {
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserService userService;

    /**
     * Retrieve the shopping cart object from session.
     * If the object doesn't already exist, an empty shopping cart is created in session.
     *
     * @param request
     * @return
     */
    private ShoppingCart getCartInSession(HttpServletRequest request) {
        ShoppingCart shoppingCart = (ShoppingCart) request.getSession().getAttribute("myCart");
        if (shoppingCart == null) {
            shoppingCart = new ShoppingCart();
            request.getSession().setAttribute("myCart", shoppingCart);
        }
        return shoppingCart;
    }

    @GetMapping("/shopping-cart")
    public ModelAndView showShoppingCart(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView("cart/checkout");
        modelAndView.addObject("cart", getCartInSession(request));
        User user = userService.curentUser();
        modelAndView.addObject("loggedInUser", user);

        return modelAndView;
    }

    @GetMapping("/shopping-cart/remove-product/{id}")
    public String removeProduct(@PathVariable("id") Long id, HttpServletRequest request) {
        ShoppingCartService shoppingCartService = new ShoppingCartService(getCartInSession(request), productRepository);
        shoppingCartService.deleteProductById(id);
        request.getSession().setAttribute("myCart", shoppingCartService.getShoppingCart());

        return "redirect:/shopping-cart";
    }

    @GetMapping("/shopping-cart/add-product/{id}")
    public String addProduct(@PathVariable("id") Long id, HttpServletRequest request) {
        ShoppingCartService shoppingCartService = new ShoppingCartService(getCartInSession(request), productRepository);
        shoppingCartService.addProductById(id);
        request.getSession().setAttribute("myCart", shoppingCartService.getShoppingCart());

        return "redirect:/shopping-cart";
    }
}

