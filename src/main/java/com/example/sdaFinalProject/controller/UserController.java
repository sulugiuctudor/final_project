package com.example.sdaFinalProject.controller;

import com.example.sdaFinalProject.model.User;
import com.example.sdaFinalProject.repository.UserRepository;
import com.example.sdaFinalProject.security.adapter.UserDetailsAdapter;
import com.example.sdaFinalProject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;



@Controller
@RequestMapping
public class UserController {
    @Autowired
    private UserRepository userRepository;
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Page users list
    @GetMapping("/users")
    public ModelAndView showUsers() {
        ModelAndView modelAndView = new ModelAndView("user/list");

        List<User> listUsers = userRepository.findAll();
        for (User user : listUsers) {

            if (!curentUser().getRole().equals("ADMIN")) {
                if(user.getId() == curentUser().getId()){
                    listUsers.clear();
                    List<User> tempListUser = new ArrayList<>();
                    tempListUser.add(user);
                    modelAndView.addObject("users", tempListUser);
                    return modelAndView;
                }

            }else{
                modelAndView.addObject("users", listUsers);
                return modelAndView;
            }
        }
        modelAndView.addObject("users", listUsers);
        return modelAndView;
    }


    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Delete User


    @GetMapping("/delete-user/{id}")
    public ModelAndView deleteUser(@PathVariable long id) {
        ModelAndView modelAndView = new ModelAndView("redirect:/users");
        userService.deleteUser(id);
        return modelAndView;
    }


    @DeleteMapping("/delete/{id}")
    public void deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
    }

    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Register User

    @PostMapping("/register")
    public String register(@ModelAttribute("createUserForm") @Valid User user, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "user/update";
        }
        userService.createUser(user);
        return "redirect:/index";
    }

    @GetMapping("/register")
    public ModelAndView getRegisterForm(ModelMap modelmap) {
        modelmap.addAttribute("createUserForm", new User());
        return new ModelAndView("user/new");
    }

    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Update User

/*    @GetMapping("/update-user/{id}")
    public ModelAndView getUsersPage(@PathVariable long id) {
        ModelAndView mav = new ModelAndView("user/update");
        User user = userRepository.findById(id).get();
        mav.addObject("updateUserForm", user);
        return mav;
    }*/



    @GetMapping("/save-update-user/{id}")
    public ModelAndView updateUser(@PathVariable("id") long id, ModelMap modelMap) {
        modelMap.addAttribute("updateUserForm",
                userRepository.findById(id));
        return new ModelAndView("user/update");
    }
    @PostMapping("/save-update-user/{id}")
    public String updateUser(@ModelAttribute("updateUserForm") @Valid User user, BindingResult bindingResult,
                             @PathVariable("id") long id) {

        if (bindingResult.hasErrors()) {
            return "user/update";
        }

        userService.updateUser(user, id);
        return "redirect:/users";

    }

    public static User curentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsAdapter userDetailsAdapter = (UserDetailsAdapter) authentication.getPrincipal();
        return userDetailsAdapter.getUser();
    }

}
