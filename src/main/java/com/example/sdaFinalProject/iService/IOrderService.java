package com.example.sdaFinalProject.iService;

import com.example.sdaFinalProject.model.Order;
import java.util.List;

public interface IOrderService {

    List<Order> getOrders();
    Order createOrder(Order order);
    void updateOrder(Order Order);
    void deleteOrder(Long id);
}
