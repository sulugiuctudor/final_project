package com.example.sdaFinalProject.iService;

public interface IShoppingCartService {

    void addProductById(Long id);
    void deleteProductById(Long id);
    void calculateTotalOrderValue();

}
