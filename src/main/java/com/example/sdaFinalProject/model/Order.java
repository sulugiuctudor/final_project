package com.example.sdaFinalProject.model;

import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "orders")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_id")
    private long orderId;
    @Column(name = "total_order_price")
    private Double totalValue;
    @Column(name = "order_status")
    private String status;
    @Column(name = "order_date")
    private Date orderDate;
    @Column(name = "shipping_name")
    private String shippingName;
    @Column(name = "shipping_address")
    private String shippingAddress;

    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "clientId")
    private User user;

    @ToString.Exclude
    @OneToMany(mappedBy = "order", cascade = {CascadeType.ALL})
    private Set<OrderProduct> orderProductSet = new HashSet<>();

}
