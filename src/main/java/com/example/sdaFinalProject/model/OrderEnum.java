package com.example.sdaFinalProject.model;

public enum OrderEnum {

    PLACED,
    IN_PROGRESS,
    DELIVERED
}
