package com.example.sdaFinalProject.model;

import lombok.Data;
import lombok.ToString;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name = "products")
@Data
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Lob
    @Column(name = "photo", columnDefinition = "MEDIUMBLOB")
    private String photo;

    @NotNull(message = "is required")
    @Size(min=1, message = "is required")
    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private Double price;

    @Column(name = "description", length = 10000)
    private String description;

    @Column(name = "dimensions")
    private String dimensions;

    @Column(name = "materials")
    private String materials;

    @Column(name = "colour")
    private String colour;


    public Product() {
    }

    public Product(String photo, String name, Double price, String description, String dimensions, String materials, String colour, Set<OrderProduct> orderProductSet) {
        this.photo = photo;
        this.name = name;
        this.price = price;
        this.description = description;
        this.dimensions = dimensions;
        this.materials = materials;
        this.colour = colour;
        this.orderProductSet = orderProductSet;
    }

    @ToString.Exclude
    @OneToMany(mappedBy = "product")
    private Set<OrderProduct> orderProductSet = new HashSet<>();
}