package com.example.sdaFinalProject.model;

import javax.persistence.Entity;

public enum Role {
    EMPLOYEE,
    USER
}
