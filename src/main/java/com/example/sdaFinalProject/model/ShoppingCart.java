package com.example.sdaFinalProject.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Component
public class ShoppingCart {

    private List<ShoppingCartProduct> shoppingCartProducts = new ArrayList<>();
    private Double totalValue;
    private String shippingName;
    private String shippingAddress;
}
