package com.example.sdaFinalProject.model;

import lombok.Data;

import java.util.Objects;

@Data
public class ShoppingCartProduct {

    private Product product;

    private Integer quantity;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShoppingCartProduct that = (ShoppingCartProduct) o;
        return product.getId() == that.getProduct().getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(product);
    }
}
