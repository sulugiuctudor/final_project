package com.example.sdaFinalProject.model;


import com.example.sdaFinalProject.validator.user.userCnp.ValidateUserCnp;
import com.example.sdaFinalProject.validator.user.userDateEmployee.ValidateUserDateEmployee;
import com.example.sdaFinalProject.validator.user.userPhoneNumber.ValidateUserPhoneNumber;
import com.example.sdaFinalProject.validator.user.userName.ValidateUserName;

import lombok.*;

import javax.persistence.*;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "users")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private long id;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", address='" + address + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", dateEmployee='" + dateEmployee + '\'' +
                ", cnp='" + cnp + '\'' +
                ", role='" + role + '\'' +
                ", orderSet=" + orderSet +
                '}';
    }

    @Column(name = "first_name")
    @ValidateUserName
    private String firstName;
    @Column(name = "second_name")
    @ValidateUserName
    private String secondName;
    @Column(name = "phone_number")
    @ValidateUserPhoneNumber
    private String phoneNumber;
    @Column(name = "address")
    private String address;
    @Column(name = "username")
    private String username;
    @Column(name = "password")
    private String password;
    @Column(name = "email")
    private String email;
    @Column(name = "date_employee")
    @ValidateUserDateEmployee
    private String dateEmployee;
    @Column(name = "cnp")
    @ValidateUserCnp
    private String cnp;
    @Column(name ="role")
    private String role;


    @ToString.Exclude
    @OneToMany(mappedBy = "user", cascade = {CascadeType.ALL})
    private Set<Order> orderSet = new HashSet<>();

}
