package com.example.sdaFinalProject.repository;

import com.example.sdaFinalProject.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ProductRepository extends JpaRepository<Product, Long> {
    public Product findByName(String name);


}
