package com.example.sdaFinalProject.service;

import com.example.sdaFinalProject.iService.IOrderService;
import com.example.sdaFinalProject.model.Order;
import com.example.sdaFinalProject.repository.OrderRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService implements IOrderService {
    private final OrderRepository orderRepository;

    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public List<Order> getOrders() {
        return orderRepository.findAll();
    }

    @Override
    public Order createOrder(Order order) {
        return orderRepository.save(order);
    }

    @Override
    public void updateOrder(Order order) {
        Order orderToUpdate = orderRepository.findById(order.getOrderId())
                .orElseThrow(() -> new IllegalStateException(toString().formatted("Order doesn't exist!")));

        orderToUpdate.setStatus(order.getStatus());
        orderToUpdate.setShippingName(order.getShippingName());
        orderToUpdate.setShippingAddress(order.getShippingAddress());
        orderToUpdate.setOrderProductSet(order.getOrderProductSet());

        orderRepository.save(orderToUpdate);
    }

    @Override
    public void deleteOrder(Long id) {
        boolean orderExists = orderRepository.existsById(id);
        if (!orderExists) {
            throw new IllegalStateException(String.format("Order with id &s does not exist", id));
        }
        orderRepository.deleteById(id);
    }

}
