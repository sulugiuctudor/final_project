package com.example.sdaFinalProject.service;

import com.example.sdaFinalProject.model.Product;
import com.example.sdaFinalProject.repository.ProductRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
@Service
public class ProductService {

    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public void createProduct(Product product) {
        productRepository.save(product);
    }

    //------------Create Product with Validation-----------
    public void createProduct(Product product, MultipartFile file) {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        if (fileName.contains("..")) {
            System.out.println("not a a valid file");
        }
        try {
            product.setPhoto(encodePhoto(file));
        } catch (IOException e) {
            e.printStackTrace();
        }

        productRepository.save(product);
    }

    private static String encodePhoto(MultipartFile file) throws IOException {
        return Base64.getEncoder().encodeToString(file.getBytes());
    }

    //save product
    public void saveProduct(Product product) {
        productRepository.save(product);
    }


    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    //Update Product with File
    public void updateProduct(Product product, MultipartFile file) {
        Optional<Product> foundProduct = productRepository.findById(product.getId());
        if (foundProduct.isPresent()) {
            checkBlobPicture(product, foundProduct, file);
            productRepository.save(product);
        }
    }

    // Verify checkBlobPicture with an IOException
    private static void checkBlobPicture(Product product, Optional<Product> foundProduct, MultipartFile file) {
        try {
            if (file.isEmpty()) {
                product.setPhoto(foundProduct.get().getPhoto());
            } else {
                product.setPhoto(encodePhoto(file));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
        ///Product with id does not exist
    public void deleteProduct(long id) {
        boolean productExists = productRepository.existsById(id);
        if(!productExists){
            throw new IllegalStateException(String.format("Product with id &s does not exist",id));
        }

        Product product = productRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid product Id:" + id));
        productRepository.delete(product);
    }


    public List<Product> searchAllProducts() {
        return productRepository.findAll();
    }

    public Product findProductbyName(String name) {
        return productRepository.findByName(name);
    }
}
