package com.example.sdaFinalProject.service;

import com.example.sdaFinalProject.iService.IShoppingCartService;
import com.example.sdaFinalProject.model.Product;
import com.example.sdaFinalProject.model.ShoppingCart;
import com.example.sdaFinalProject.model.ShoppingCartProduct;
import com.example.sdaFinalProject.repository.ProductRepository;
import lombok.Getter;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Getter
@Service
public class ShoppingCartService implements IShoppingCartService {

    private ProductRepository productRepository;
    private ShoppingCart shoppingCart;

    public ShoppingCartService(ShoppingCart shoppingCart, ProductRepository productRepository) {
        this.productRepository = productRepository;
        this.shoppingCart = shoppingCart;
    }

    @Override
    public void addProductById(Long id) {
        Optional<Product> product = productRepository.findById(id);
        if (product.isPresent()) {
            ShoppingCartProduct shoppingCartProduct = new ShoppingCartProduct();
            shoppingCartProduct.setProduct(product.get());
            boolean found = false;
            for (ShoppingCartProduct p : shoppingCart.getShoppingCartProducts()) {
                if (p.equals(shoppingCartProduct)) {
                    p.setQuantity(p.getQuantity() + 1);
                    found = true;
                }
            }

            if (!found) { // found == false
                shoppingCartProduct.setQuantity(1);
                shoppingCart.getShoppingCartProducts().add(shoppingCartProduct);
            }
        }
        calculateTotalOrderValue();
    }

    @Override
    public void deleteProductById(Long id) {

        for (ShoppingCartProduct p : shoppingCart.getShoppingCartProducts()) {
            if (p.getProduct().getId() == id) {
                shoppingCart.getShoppingCartProducts().remove(p);
            }
        }
        calculateTotalOrderValue();
    }

    @Override
    public void calculateTotalOrderValue() {
        shoppingCart.setTotalValue(0.0);
        for (ShoppingCartProduct p : shoppingCart.getShoppingCartProducts()) {
            Double totalValue = shoppingCart.getTotalValue() + p.getQuantity() * p.getProduct().getPrice();
            shoppingCart.setTotalValue(totalValue);
        }
    }
}
