package com.example.sdaFinalProject.service;


import com.example.sdaFinalProject.model.User;
import com.example.sdaFinalProject.repository.UserRepository;
import com.example.sdaFinalProject.security.adapter.UserDetailsAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class UserService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> getUsers() {
        return userRepository.findAll() ;
    }

    public void createUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }

    public void updateUser(User user, Long id){

        User userToUpdate = userRepository.findById(id)
                .orElseThrow(()-> new IllegalStateException(String.format("User %s %s dosen't exist", user.getFirstName(), user.getSecondName())));

//Validated fields frontend
        userToUpdate.setFirstName(user.getFirstName());
        userToUpdate.setSecondName(user.getSecondName());
        userToUpdate.setPhoneNumber(user.getPhoneNumber());
        userToUpdate.setDateEmployee(user.getDateEmployee());
        userToUpdate.setCnp(user.getCnp());
//Backend exceptions
        userToUpdate.setUsername(verifyUsernameUnique(user.getUsername(), id));
        userToUpdate.setEmail(validateEmail(user.getEmail(),id));
//Unvalidated fields
        userToUpdate.setAddress(user.getAddress());
//Frontend Validate
        userToUpdate.setRole(user.getRole());




        if(!user.getPassword().equals("")) {
            userToUpdate.setPassword(passwordEncoder.encode(user.getPassword()));
        }

        userRepository.save(userToUpdate);
    }


    public void deleteUser(Long id) {
        boolean userExists = userRepository.existsById(id);
        if(!userExists){
            throw new IllegalStateException(String.format("User with id &s does not exist",id));
        }
        userRepository.deleteById(id);
    }

    private String validateEmail(String email, Long id){
        List<User> listUser =  getUsers();
        for (User user:listUser) {
            if(email.equalsIgnoreCase(user.getEmail()) && !id.equals(user.getId())){
                throw new IllegalStateException("Email alredy exist");
            }
        }
        return email;
    }

    private String verifyUsernameUnique(String username, Long id){
        List<User> listUser =  getUsers();
        for (User user:listUser) {
            if(user.getUsername().equalsIgnoreCase(username) && user.getId()!=id){
                throw new IllegalStateException("Username alredy exist");
            }
        }
        return username;
    }

    private void validateRole(String role) {
        if(role==null){
            throw new IllegalStateException("Role can not be empty");
        }
    }





    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByUsername(username);
        User foundUser = user.orElseThrow(() -> new UsernameNotFoundException("Error! Username not found"));
        return new UserDetailsAdapter(foundUser);
    }

    public User curentUser(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsAdapter userDetailsAdapter = (UserDetailsAdapter) authentication.getPrincipal();
        User user = userDetailsAdapter.getUser();
        return user;
    }
}
