package com.example.sdaFinalProject.validator.product;

import com.example.sdaFinalProject.validator.user.userName.ValidateUserName;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class ProductDescription  implements ConstraintValidator<ValidateProductDescription, String> {

    @Override
    public void initialize(ValidateProductDescription constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String description, ConstraintValidatorContext context) {
        if(description == null || description.equals("")){
            return false;
        }
        if(description.length() > 200){
            return false;
        }
        return true;
    }
}

