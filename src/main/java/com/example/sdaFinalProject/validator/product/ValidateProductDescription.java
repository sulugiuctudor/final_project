package com.example.sdaFinalProject.validator.product;

import com.example.sdaFinalProject.validator.user.userName.UserValidatorName;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.FIELD,ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = ProductDescription.class)
public @interface ValidateProductDescription {
    String message() default  "{javax.validation.constraints.Min.message}";

    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
