package com.example.sdaFinalProject.validator.user.userCnp;


import com.example.sdaFinalProject.controller.UserController;
import com.example.sdaFinalProject.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class UserValidatorCnp implements ConstraintValidator<ValidateUserCnp, String> {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserController userController;

    @Override
    public void initialize(ValidateUserCnp constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String cnp, ConstraintValidatorContext constraintValidatorContext) {
        if (cnp.matches("^[1-9]\\d{2}(0[1-9]|1[0-2])(0[1-9]|[12]\\d|3[01])(0[1-9]|[1-4]\\d|5[0-2]|99)(00[1-9]|0[1-9]\\d|[1-9]\\d\\d)\\d$")) {

            String verifySeries = "279146358279";
            ArrayList<String> verifyList = new ArrayList<>(Arrays.asList(verifySeries.split("")));
            ArrayList<String> cnpList = new ArrayList<>(Arrays.asList(cnp.split("")));


            int p1 = Integer.parseInt(verifyList.get(0)) * Integer.parseInt(cnpList.get(0));
            int p2 = Integer.parseInt(verifyList.get(1)) * Integer.parseInt(cnpList.get(1));
            int p3 = Integer.parseInt(verifyList.get(2)) * Integer.parseInt(cnpList.get(2));
            int p4 = Integer.parseInt(verifyList.get(3)) * Integer.parseInt(cnpList.get(3));
            int p5 = Integer.parseInt(verifyList.get(4)) * Integer.parseInt(cnpList.get(4));
            int p6 = Integer.parseInt(verifyList.get(5)) * Integer.parseInt(cnpList.get(5));
            int p7 = Integer.parseInt(verifyList.get(6)) * Integer.parseInt(cnpList.get(6));
            int p8 = Integer.parseInt(verifyList.get(7)) * Integer.parseInt(cnpList.get(7));
            int p9 = Integer.parseInt(verifyList.get(8)) * Integer.parseInt(cnpList.get(8));
            int p10 = Integer.parseInt(verifyList.get(9)) * Integer.parseInt(cnpList.get(9));
            int p11 = Integer.parseInt(verifyList.get(10)) * Integer.parseInt(cnpList.get(10));
            int p12 = Integer.parseInt(verifyList.get(11)) * Integer.parseInt(cnpList.get(11));

            float s = (p1 + p2 + p3 + p4 + p5 + p6 + p7 + p8 + p9 + p10 + p11 + p12)/11;
            float c = s%2;



            if(c==Integer.parseInt(cnpList.get(12))) {
                return true;
            }
        }
        return false;
    }
    private double f(int i) {
        return i / 11 + (float)(i%2);
    }

}
