package com.example.sdaFinalProject.validator.user.userCnp;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.FIELD,ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = UserValidatorCnp.class)
public @interface ValidateUserCnp {
    String message() default  "{javax.validation.constraints.Min.message}";

    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
