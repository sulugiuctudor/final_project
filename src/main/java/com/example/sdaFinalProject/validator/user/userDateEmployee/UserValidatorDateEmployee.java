package com.example.sdaFinalProject.validator.user.userDateEmployee;

import com.example.sdaFinalProject.validator.user.userName.ValidateUserName;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.annotation.Annotation;
import java.time.LocalDate;

import static java.time.LocalTime.now;

public class UserValidatorDateEmployee implements ConstraintValidator<ValidateUserDateEmployee, String> {

    @Override
    public void initialize(ValidateUserDateEmployee constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        LocalDate date = LocalDate.parse(s);
        if (date.isBefore(LocalDate.now())){
            return true;
        }
        return false;
    }
}
