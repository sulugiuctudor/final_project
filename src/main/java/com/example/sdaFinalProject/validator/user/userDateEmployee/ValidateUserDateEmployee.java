package com.example.sdaFinalProject.validator.user.userDateEmployee;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.FIELD,ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = UserValidatorDateEmployee.class)

public @interface ValidateUserDateEmployee {
    String message() default  "{javax.validation.constraints.Min.message}";

    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
