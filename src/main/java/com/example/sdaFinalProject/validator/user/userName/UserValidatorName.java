package com.example.sdaFinalProject.validator.user.userName;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class UserValidatorName implements ConstraintValidator<ValidateUserName, String> {


    @Override
    public void initialize(ValidateUserName constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String name, ConstraintValidatorContext constraintValidatorContext) {
        if(name == null || name.equals("")){
            return false;
        }
        if(!name.matches("\\b([A-ZÀ-ÿ][-,a-z. ']+[ ]*)+")){
            return false;
        }
        return true;
    }
}
