package com.example.sdaFinalProject.validator.user.userName;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.FIELD,ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = UserValidatorName.class)
public @interface ValidateUserName {
    String message() default  "{javax.validation.constraints.Min.message}";

    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
