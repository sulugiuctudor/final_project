package com.example.sdaFinalProject.validator.user.userPhoneNumber;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class UserValidatorPhoneNumber implements ConstraintValidator<ValidateUserPhoneNumber, String> {

    @Override
    public void initialize(ValidateUserPhoneNumber constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String phoneNumber, ConstraintValidatorContext constraintValidatorContext) {
        if(!phoneNumber.matches("^(\\+4|)?(07[0-8]{1}[0-9]{1}|02[0-9]{2}|03[0-9]{2}){1}?(\\s|\\.|\\-)?([0-9]{3}(\\s|\\.|\\-|)){2}$")){
            return false;
        }
        return true;

    }
}
