package com.example.sdaFinalProject.validator.user.userPhoneNumber;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.FIELD,ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = UserValidatorPhoneNumber.class)
public @interface ValidateUserPhoneNumber {
    String message() default  "{javax.validation.constraints.Min.message}";

    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
