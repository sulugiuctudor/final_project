package com.example.sdaFinalProject;

import com.example.sdaFinalProject.model.Order;
import com.example.sdaFinalProject.model.OrderProduct;
import com.example.sdaFinalProject.model.Product;
import com.example.sdaFinalProject.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class SdaFinalProjectApplicationTests {

    private static final Set<Order> orderSetUser = new HashSet<>();
    private static final Set<OrderProduct> orderSetProduct = new HashSet<>();

    @Test
    void checkUserFieldsCreation() {
        User newUser = new User(5965656, "sor", "mor", "90128390123", "Str caca", "sor28", "12345",
                "sor@gmail.com", "29.03.2022", "123344442141243", "bucatar", orderSetUser);

        assertEquals(newUser.getUsername(), "sor28");
        assertEquals(newUser.getRole(), "bucatar");
    }

    @Test
    void checkProductsAfterAddingOne() {
        Product newProduct = new Product("1", "canapeaBuna", 25.55, "stofa", "1222x2333x2324", "piele,stofa,matlasa", "blue", orderSetProduct);

        assertEquals(newProduct.getPrice(), 25.55);
        assertEquals(newProduct.getColour(), "blue");
        assertNotNull(newProduct.getPhoto());
    }

}
